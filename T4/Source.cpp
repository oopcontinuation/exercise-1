#include <iostream>
using namespace std;
class Perittava {};
class PerittyA : public Perittava {};
class PerittyB : public Perittava {};
void main(void)
{
	Perittava *pk1 = new PerittyA;
	Perittava *pk2 = new PerittyA;
	PerittyA *pPA;
	PerittyB *pPB;
	pPA = static_cast<PerittyA*>(pk1); // 1.
	pPB = (PerittyB *)pk2; //static_cast<PerittyB*>(pk2); // 2.
	delete pk1;
	delete pk2;
	void *ppii = new double(3.14);
	double *pd;
	pd = (double *)ppii; // 3.
	delete ppii;
}