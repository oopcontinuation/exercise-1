// Harjoitus1.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <memory>

#include "henkilo.h"
#include "sub_henkilo.h"
int main(){

	std::vector<Henkilo*> henkilot;

	henkilot.push_back(new Henkilo("EN1", "SN1", "S1"));
	henkilot.push_back(new Opettaja("EN2", "SN2", "S2", "t"));
	henkilot.push_back(new Opiskelija("EN3", "SN3", "S3", "y"));

	int c = 0;
	for (unsigned i = 0; i < henkilot.size(); i++) {
		if (dynamic_cast<Opiskelija*> (henkilot[i]) != NULL) c++;
	}
	
	std::cout << "Opiskelijoita: " << c << std::endl;

	std::string s;

	std::cout << s;

	getchar();
    return 0;
}

