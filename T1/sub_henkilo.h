#include "henkilo.h"

class Opiskelija: public Henkilo{
public:
	Opiskelija(std::string, std::string, std::string);
	Opiskelija(std::string, std::string, std::string, std::string);
	std::string getInfo();
	std::string opnumero;
};

class Opettaja: public Henkilo{
public:
	Opettaja(std::string, std::string, std::string);
	Opettaja(std::string, std::string, std::string, std::string);
	std::string getInfo();
	std::string osaamiskeskus;
};