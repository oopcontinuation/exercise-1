#include "sub_henkilo.h"


Opiskelija::Opiskelija(std::string en, std::string sn, std::string s) : Opiskelija(en, sn, s, "") {};
Opiskelija::Opiskelija(std::string en, std::string sn, std::string s, std::string op) {
	Henkilo(en, sn, s);
	this->opnumero = op;
};
std::string Opiskelija::getInfo() {
	return etunimi + " " + sukunimi + " " + sotu + " " + opnumero;

};

Opettaja::Opettaja(std::string en, std::string sn, std::string s) : Opettaja(en, sn, s, "") {};
Opettaja::Opettaja(std::string en, std::string sn, std::string s, std::string ok) {
	Henkilo(en, sn, s);
	this->osaamiskeskus = ok;
};
std::string Opettaja::getInfo() {
	return etunimi + " " + sukunimi + " " + sotu + " " + osaamiskeskus;

};
