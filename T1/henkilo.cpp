#pragma once
#include "henkilo.h"

Henkilo::Henkilo() {};
Henkilo::Henkilo(std::string en, std::string sn, std::string s) {
	this->etunimi = en;
	this->sukunimi = sn;
	this->sotu = s;
};

Henkilo& Henkilo::operator=(const std::string& o) {
	unsigned int pos = o.find(" ");
	unsigned int initialPos = 0;
	std::vector<std::string> ta;

	// Parse through each word in o, pushing to vector ta
	while (pos != std::string::npos) {
		ta.push_back(o.substr(initialPos, pos - initialPos + 1));
		initialPos = pos + 1;

		pos = o.find(" ", initialPos);
	}
	ta.push_back(o.substr(initialPos, std::min(pos, o.size()) - initialPos + 1));

	int tas = ta.size();
	if (tas >= 1) {
		this->etunimi = ta[0];
	}
	if (tas >= 2) {
		this->sukunimi = ta[1];
	}
	if (tas >= 3) {
		this->sotu = ta[2];
	}
	return *this;
};


Henkilo::operator std::string() {
	return etunimi + " " + sukunimi + " " + sotu;
}

std::string Henkilo::getInfo() {
	return etunimi + " " + sukunimi + " " + sotu;
};