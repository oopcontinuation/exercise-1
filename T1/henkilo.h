#pragma once
#include <iostream>
#include <algorithm>
#include <vector>
#include <string>

class Henkilo {
public:
	std::string etunimi;
	std::string sukunimi;
	std::string sotu;

	Henkilo();
	Henkilo(std::string, std::string, std::string);

	Henkilo& operator=(const std::string& o);
	operator std::string();
	virtual std::string getInfo();
private:
	
protected:

};
